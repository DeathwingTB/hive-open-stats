import { ConfigModule } from '@nestjs/config'
import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { SequelizeModule } from '@nestjs/sequelize'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { CollectorService } from './stats/collector.service'
import { HiveSqlService } from './hivesql/hiveSql.service'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.dev', '.env']
    }),
    ScheduleModule.forRoot(),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      uri: process.env.DATABASE_URL,
      synchronize: true,
      models: [],
      name: 'localConnection',
      dialectOptions: {
        ssl: {
          require: true,
          rejectUnauthorized: false
        }
      }
    }),
    SequelizeModule.forRoot({
      dialect: 'mssql',
      uri: process.env.HIVESQL_DATABASE_URL,
      synchronize: true,
      name: 'hivesqlConnection',
      models: []
    })
  ],
  controllers: [AppController],
  providers: [AppService, CollectorService, HiveSqlService]
})
export class AppModule {}
