import { Injectable } from '@nestjs/common'

const packageJson = require('../package.json')

@Injectable()
export class AppService {
  status(): any {
    return {
      app: packageJson.name,
      version: packageJson.version
    }
  }
}
