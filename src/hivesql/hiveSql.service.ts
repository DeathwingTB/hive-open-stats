import { Injectable } from '@nestjs/common'
import { InjectConnection } from '@nestjs/sequelize'
import { QueryTypes, Sequelize } from 'sequelize'

@Injectable()
export class HiveSqlService {
  constructor(
    @InjectConnection('hivesqlConnection')
    private sequelize: Sequelize
  ) {}

  async executeQuery(query: string, params: any[]): Promise<any[]> {
    return await this.sequelize.query(query, {
      replacements: params,
      type: QueryTypes.SELECT
    })
  }
}
