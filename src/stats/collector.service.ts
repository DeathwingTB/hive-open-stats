import { Injectable } from '@nestjs/common'
import { Cron } from '@nestjs/schedule'

@Injectable()
export class CollectorService {
  @Cron('0 * * * *')
  async hourly() {
    // load hourly stats
    // loop and run queries
    // store results
  }

  @Cron('0 8 * * *')
  async daily() {
    // load daily stats
    // loop and run queries
    // store results
  }
}
