import { NestFactory } from '@nestjs/core'
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify'
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger'

import { AppModule } from './app.module'

const packageJson = require('../package.json')

declare const module: any

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(AppModule, new FastifyAdapter(), { logger: ['error', 'warn', 'log', 'debug'] })

  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }

  // api docs
  const swaggerConfig = new DocumentBuilder().setTitle(packageJson.name).setDescription(packageJson.description).setVersion(packageJson.version).build()
  const swaggerDoc = SwaggerModule.createDocument(app, swaggerConfig)
  SwaggerModule.setup('docs', app, swaggerDoc)

  // options
  app.enableCors()

  // listen
  await app.listen(process.env.PORT || 3000)
  console.log(`Application is running on: ${await app.getUrl()}`)
}

bootstrap()
